package com.project.mcif;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class McifApplication {

	public static void main(String[] args) {
		SpringApplication.run(McifApplication.class, args);
	}

}
