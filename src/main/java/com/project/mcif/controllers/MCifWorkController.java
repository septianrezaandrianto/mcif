package com.project.mcif.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.project.mcif.dtos.MCifWorkDto;
import com.project.mcif.models.parameter.Response;
import com.project.mcif.services.MCifWorkService;

@RestController
@RequestMapping("/mCifWork")
public class MCifWorkController {

	@Autowired
	private MCifWorkService service;
	
	
	@PostMapping("/create")
	public Map<String, Object> create(@RequestBody MCifWorkDto mCifWorkDto) {
		return service.create(mCifWorkDto);
	}
	
	@GetMapping("/getAll")
	public Map<String, Object> getAll() {
		return service.getAll();
	}
	
	@GetMapping("/getById")
	public Map<String, Object> getById(@RequestParam("id") Long id) {
		return service.getById(id);
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> update(@PathVariable("id") Long id, @RequestBody MCifWorkDto mCifWorkDto) {
		return service.update(id, mCifWorkDto);
	}
	
	@DeleteMapping("/delete")
	public Map<String, Object> delete(@RequestParam("id") Long id) {
		return service.delete(id);
	}
	
	@GetMapping("/getDataAll")
	public Response getDataAll() {
		return service.getDataAll();
	}
}
