package com.project.mcif.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.mcif.dtos.MCifDto;
import com.project.mcif.models.MCif;
import com.project.mcif.models.parameter.MCifRequest;
import com.project.mcif.services.MCifService;

@RestController
@RequestMapping("/mCif")
public class MCifController {

	@Autowired
	private MCifService service;
	
	@PostMapping("/create")
	public Map<String, Object> create(@RequestBody MCifRequest mCifRequest) {
		return service.create(mCifRequest);
	}
}
