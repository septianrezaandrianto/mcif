package com.project.mcif.models;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity(name="mCif")
@Table(name="m_cif", schema = "public")
@EntityListeners(AuditingEntityListener.class)
public class MCif implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_mcif_id_seq")
	@SequenceGenerator(name="generator_mcif_id_seq", sequenceName="mcif_id_seq", schema = "public", allocationSize = 1)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;
	
	@Column(name = "id_card")
	private String idCard;
	
	@Column(name = "name")
	private String name;

	@Column(name = "npwp")
	private String npwp;
	
	@Column(name = "no_telepon")
	private String noTelepon;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "type")
	private String type;
	
	@Column(name = "created_by")
    @CreatedBy
    private String createdBy;

    @Column(name = "created_on", columnDefinition = "DATE")
    @CreatedDate
    private Timestamp createdOn;

    @Column(name = "last_modified_by")
    @LastModifiedBy
    private String lastModifiedBy;

    @Column(name = "last_modified_on", columnDefinition = "DATE")
    @LastModifiedDate
    private Timestamp lastModifiedOn;
    
    @Column(name = "is_deleted")
    private Boolean isDeleted;
    
    @Column(name="pekerjaan")
    private String pekerjaan;

}
