package com.project.mcif.models.parameter;

import java.sql.Timestamp;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MCifRequest {

	private String idCard;
	private String name;
	private String npwp;
	private String noTelepon;
	private String email;
	private String type;
	private String createdBy;
	private Timestamp createdOn;
	private String lastModifiedBy;
	private Timestamp lastModifiedOn;
	private Boolean isDeleted;
	private List<String> pekerjaan;
}
