package com.project.mcif.models.parameter;

import lombok.Data;

@Data
public class Response {

	private String response;
	private String message;
	private Integer total;
	private Object data;
	
}
