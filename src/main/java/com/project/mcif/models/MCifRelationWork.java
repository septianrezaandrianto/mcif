package com.project.mcif.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_cif_relation_work")
public class MCifRelationWork implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_mcif_relation_work_id_seq")
	@SequenceGenerator(name="generator_mcif_relation_work_id_seq", sequenceName="mcif_relation_work_id_seq", schema = "public", allocationSize = 1)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_m_cif")
	private MCif idMCif;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_m_cif_work")
	private MCifWork idMCifWork;
}
