package com.project.mcif.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.project.mcif.constant.Constant;
import com.project.mcif.dtos.MCifWorkDto;
import com.project.mcif.models.MCifWork;
import com.project.mcif.models.parameter.Response;
import com.project.mcif.repositories.MCifWorkRepository;

@Service
public class MCifWorkServiceImpl implements MCifWorkService {

	@Autowired
	private MCifWorkRepository repository;

	ModelMapper mapper = new ModelMapper();
	
	private MCifWork convertToEntity(MCifWorkDto mCifWorkDto) {
		return mapper.map(mCifWorkDto, MCifWork.class);
	}
	
	private MCifWorkDto convertToDto(MCifWork mCifWork) {
		return mapper.map(mCifWork, MCifWorkDto.class);
	}
	
	@Override
	public Map<String, Object> create(MCifWorkDto mCifWorkDto) {
		Map<String, Object> result = new HashMap<>();
		
		MCifWork mCifWork = convertToEntity(mCifWorkDto);
		mCifWork.setIsDeleted(false);
		
		MCifWork resultInput = repository.save(mCifWork);
		
		result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
		result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
		result.put(Constant.DATA_STRING, resultInput);
		return result;
	}

	
	@Override
	public Map<String, Object> getAll() {
		Map<String, Object> result = new HashMap<>();
		List<MCifWorkDto> listData = new ArrayList<>();
		
		for (MCifWork data : repository.findAll()) {
			if (!data.getIsDeleted()) {
				MCifWorkDto mCifWorkDto = convertToDto(data);
				listData.add(mCifWorkDto);
			}
		}
		String message = "";
		if (!listData.isEmpty()) {
			message = Constant.SUCCESS_STRING;
		}
		else {
			message = Constant.EMPTY_DATA_STRING;
		}
		
		result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
		result.put(Constant.MESSAGE_STRING, message);
		result.put(Constant.DATA_STRING, listData);
		result.put(Constant.TOTAL_STRING, listData.size());
		return result;
	}

	
	@Override
	public Response getDataAll() {
		Response result = new Response();
		
		List<MCifWorkDto> listData = new ArrayList<>();
		
		for (MCifWork data : repository.findAll()) {
			if (!data.getIsDeleted()) {
				MCifWorkDto mCifWorkDto = convertToDto(data);
				listData.add(mCifWorkDto);
			}
		}
		String message = "";
		if (!listData.isEmpty()) {
			message = Constant.SUCCESS_STRING;
		}
		else {
			message = Constant.EMPTY_DATA_STRING;
		}
		
		result.setMessage(message);
		result.setTotal(listData.size());
		result.setData(listData);
		result.setResponse("OK");
		return result;
	}

	
	@Override
	public Map<String, Object> getById(Long id) {
		Map<String, Object> result = new HashMap<>();
		String message = "";
		try {
			MCifWorkDto mCifWorkDto = new MCifWorkDto();
			
			MCifWork data = repository.findByIdTrue(id);
			
			mCifWorkDto = convertToDto(data);
			message = Constant.SUCCESS_STRING;
			
			result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
			result.put(Constant.MESSAGE_STRING, message);
			result.put(Constant.DATA_STRING, mCifWorkDto);
		}
		catch (Exception e) {
			result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
			result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
		}
		return result;
	}

	
	@Override
	public Map<String, Object> update(Long id, MCifWorkDto mCifWorkDto) {
		Map<String, Object> result = new HashMap<>();
		try {
			MCifWork data = repository.findByIdTrue(id);
			MCifWork mCifWork = convertToEntity(mCifWorkDto);
			
			data.setAddress(mCifWork.getAddress());
			data.setName(mCifWork.getName());
			data.setPenghasilan(mCifWork.getPenghasilan());
			
			MCifWork resultInput = repository.save(data);
			
			result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
			result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
			result.put(Constant.DATA_STRING, resultInput);
		}
		catch (Exception e) {
			result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
			result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
		}
		return result;
	}

	@Override
	public Map<String, Object> delete(Long id) {
		Map<String, Object> result = new HashMap<>();		
		try {
			MCifWork data = repository.findByIdTrue(id);
			data.setIsDeleted(true);
			
			MCifWork resultInput = repository.save(data);
			result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
			result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
			result.put(Constant.DATA_STRING, resultInput);
		}
		catch (Exception e) {
			result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
			result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
		}
		return result;
	}
		
	
}
