package com.project.mcif.services;

import java.util.HashMap;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.project.mcif.constant.Constant;
import com.project.mcif.dtos.MCifDto;
import com.project.mcif.models.MCif;
import com.project.mcif.models.parameter.MCifRequest;
import com.project.mcif.repositories.MCifRepository;

@Service
public class MCifServiceImpl implements MCifService {

	@Autowired
	private MCifRepository repository;

	ModelMapper mapper = new ModelMapper();
	
	private MCifDto convertToDto(MCif mCif) {
		return mapper.map(mCif, MCifDto.class);
	}
	
	private MCif convertToEntity(MCifDto mCifDto) {
		return mapper.map(mCifDto, MCif.class);
	}
	
	@Override
	public Map<String, Object> create(MCifRequest mCifRequest) {
		Map<String, Object> result = new HashMap<>();
		MCif mcif = new MCif();
		mcif.setIdCard(mCifRequest.getIdCard());
		mcif.setEmail(mCifRequest.getEmail());
		mcif.setName(mCifRequest.getEmail());
		mcif.setNoTelepon(mCifRequest.getNoTelepon());
		mcif.setNpwp(mCifRequest.getNpwp());
		mcif.setIsDeleted(false);
		
		Gson gson = new Gson();
		String object = gson.toJson(mCifRequest.getPekerjaan());
		
		MCif inputResult = new MCif();
		mcif.setPekerjaan(object);
		if (mCifRequest.getPekerjaan().size() <=5) {
			inputResult = repository.save(mcif);
		}
		else {
			inputResult = null;
		}
		
		result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
		result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
		result.put(Constant.DATA_STRING, inputResult);
		return result;
	}
	
	
}
