package com.project.mcif.services;

import java.util.Map;

import com.project.mcif.dtos.MCifDto;
import com.project.mcif.models.parameter.MCifRequest;

public interface MCifService {

	Map<String, Object> create(MCifRequest mCifRequest);
}
