package com.project.mcif.services;

import java.util.Map;

import com.project.mcif.dtos.MCifWorkDto;
import com.project.mcif.models.MCifWork;
import com.project.mcif.models.parameter.Response;

public interface MCifWorkService {

	Map<String, Object> create(MCifWorkDto mciCifWorkDto);
	Map<String, Object> getAll();
	Map<String, Object> getById(Long id);
	Map<String, Object> update(Long id, MCifWorkDto mCifWorkDto);
	Map<String, Object> delete(Long id);
	Response getDataAll();
}
