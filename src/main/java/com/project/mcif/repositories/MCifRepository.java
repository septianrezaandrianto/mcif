package com.project.mcif.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.mcif.models.MCif;

@Repository
public interface MCifRepository extends JpaRepository<MCif, Long> {

}
