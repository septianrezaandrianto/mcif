package com.project.mcif.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.project.mcif.models.MCifWork;

@Repository
public interface MCifWorkRepository extends JpaRepository<MCifWork, Long> {

	@Query(value="SELECT * FROM m_cif_work mcw "
			+ "WHERE mcw.id = :id "
			+ "AND mcw.is_deleted = false", nativeQuery= true)
	MCifWork findByIdTrue(@Param("id") Long id);
}
