package com.project.mcif.dtos;

import javax.persistence.Column;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MCifRelationWorkDto {

	private Long id;
	private MCifDto idMCif;
	private MCifWorkDto idMCifWork;
}
