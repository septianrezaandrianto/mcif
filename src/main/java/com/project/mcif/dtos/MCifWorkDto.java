package com.project.mcif.dtos;

import java.math.BigDecimal;
import java.sql.Timestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MCifWorkDto {

	private Long id;
	private String name;
	private String address;
	private BigDecimal penghasilan;
	private String createdBy;
	private Timestamp createdOn;
	private String lastModifiedBy;
	private Timestamp lastMmodifiedOn;
	private Boolean isDeleted;
	
}
