package com.project.mcif.dtos;

import java.sql.Timestamp;

import com.project.mcif.models.MCifWork;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MCifDto {

	private Long id;
	private String idCard;
	private String name;
	private String npwp;
	private String noTelepon;
	private String email;
	private String type;
	private String createdBy;
	private Timestamp createdOn;
	private String lastModifiedBy;
	private Timestamp lastModifiedOn;
	private Boolean isDeleted;
	private String pekerjaan;
	
}
