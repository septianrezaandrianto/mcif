/*
 Navicat Premium Data Transfer

 Source Server         : local-db
 Source Server Type    : PostgreSQL
 Source Server Version : 130002
 Source Host           : localhost:5432
 Source Catalog        : m_cif_db
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 130002
 File Encoding         : 65001

 Date: 29/07/2021 23:00:05
*/


-- ----------------------------
-- Sequence structure for mcif_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."mcif_id_seq";
CREATE SEQUENCE "public"."mcif_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER SEQUENCE "public"."mcif_id_seq" OWNER TO "postgres";

-- ----------------------------
-- Sequence structure for mcif_relation_work_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."mcif_relation_work_id_seq";
CREATE SEQUENCE "public"."mcif_relation_work_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER SEQUENCE "public"."mcif_relation_work_id_seq" OWNER TO "postgres";

-- ----------------------------
-- Sequence structure for mcif_work_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."mcif_work_id_seq";
CREATE SEQUENCE "public"."mcif_work_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER SEQUENCE "public"."mcif_work_id_seq" OWNER TO "postgres";

-- ----------------------------
-- Table structure for m_cif
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_cif";
CREATE TABLE "public"."m_cif" (
  "id" int8 NOT NULL,
  "id_card" varchar(50) COLLATE "pg_catalog"."default",
  "name" varchar(50) COLLATE "pg_catalog"."default",
  "npwp" varchar(50) COLLATE "pg_catalog"."default",
  "no_telepon" varchar(20) COLLATE "pg_catalog"."default",
  "email" varchar(50) COLLATE "pg_catalog"."default",
  "type" varchar(20) COLLATE "pg_catalog"."default",
  "is_deleted" bool,
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "created_on" date,
  "last_modified_by" varchar(255) COLLATE "pg_catalog"."default",
  "last_modified_on" date,
  "pekerjaan" varchar(255) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."m_cif" OWNER TO "postgres";

-- ----------------------------
-- Records of m_cif
-- ----------------------------
BEGIN;
INSERT INTO "public"."m_cif" VALUES (5, 'ID-001', 'dummy@gmail.com', 'npwp-001', '08561110000', 'dummy@gmail.com', NULL, 'f', NULL, '2021-07-29', NULL, '2021-07-29', '["Satpam","Bos","OB","Pelajar","Mahasiswa"]');
COMMIT;

-- ----------------------------
-- Table structure for m_cif_relation_work
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_cif_relation_work";
CREATE TABLE "public"."m_cif_relation_work" (
  "id" int8 NOT NULL,
  "id_m_cif" int8,
  "id_m_cif_work" int8
)
;
ALTER TABLE "public"."m_cif_relation_work" OWNER TO "postgres";

-- ----------------------------
-- Table structure for m_cif_work
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_cif_work";
CREATE TABLE "public"."m_cif_work" (
  "id" int8 NOT NULL,
  "name" varchar(50) COLLATE "pg_catalog"."default",
  "address" text COLLATE "pg_catalog"."default",
  "penghasilan" numeric,
  "is_deleted" bool,
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "created_on" date,
  "last_modified_by" varchar(255) COLLATE "pg_catalog"."default",
  "last_modified_on" date
)
;
ALTER TABLE "public"."m_cif_work" OWNER TO "postgres";

-- ----------------------------
-- Records of m_cif_work
-- ----------------------------
BEGIN;
INSERT INTO "public"."m_cif_work" VALUES (6, 'Solihin', 'Padang', 20000, 't', NULL, '2021-07-29', NULL, '2021-07-29');
INSERT INTO "public"."m_cif_work" VALUES (5, 'Dimas Buluk', 'Bekasi', 5000, 'f', NULL, '2021-07-29', NULL, '2021-07-29');
COMMIT;

-- ----------------------------
-- Table structure for mcif_relation_work
-- ----------------------------
DROP TABLE IF EXISTS "public"."mcif_relation_work";
CREATE TABLE "public"."mcif_relation_work" (
  "id" int8 NOT NULL,
  "id_m_cif" int8,
  "id_m_cif_work" int8
)
;
ALTER TABLE "public"."mcif_relation_work" OWNER TO "postgres";

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."mcif_id_seq"', 6, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."mcif_relation_work_id_seq"', 2, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."mcif_work_id_seq"', 7, true);

-- ----------------------------
-- Primary Key structure for table m_cif
-- ----------------------------
ALTER TABLE "public"."m_cif" ADD CONSTRAINT "m_cif_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table m_cif_relation_work
-- ----------------------------
ALTER TABLE "public"."m_cif_relation_work" ADD CONSTRAINT "m_cif_relation_work_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table m_cif_relation_work
-- ----------------------------
ALTER TABLE "public"."m_cif_relation_work" ADD CONSTRAINT "fkbdqmuhfo5qfg6jpc85m15f4j7" FOREIGN KEY ("id_m_cif") REFERENCES "public"."m_cif" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."m_cif_relation_work" ADD CONSTRAINT "fkscvh3pamcx19a93k9n6w3kw09" FOREIGN KEY ("id_m_cif_work") REFERENCES "public"."m_cif_work" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Primary Key structure for table m_cif_work
-- ----------------------------
ALTER TABLE "public"."m_cif_work" ADD CONSTRAINT "m_cif_work_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table mcif_relation_work
-- ----------------------------
ALTER TABLE "public"."mcif_relation_work" ADD CONSTRAINT "mcif_relation_work_pkey" PRIMARY KEY ("id");
